#!/usr/bin/env zsh

SCRIPT_SOURCE_DIR="${0:a:h}"
source ${SCRIPT_SOURCE_DIR}/lib-macos.sh
source ${SCRIPT_SOURCE_DIR}/macos-sign-env.sh


print_usage () {
    printf "USAGE:
  macos-notarize.sh [-s=<identity>] [-notarize-ac=<apple-account>] [-style=<style.txt>] [-bg=<background-image>]

    -f \t\t\t krita.app directory
    -s \t\t\t Code sign identity for codesign

    -notarize-ac \t Apple account name for notarization purposes
\t\t\t script will attempt to get password from keychain, if fails provide one with
\t\t\t the -notarize-pass option: To add a password run

\t\t\t   security add-generic-password -a \"AC_USERNAME\" -w <secret_password> -s \"AC_PASSWORD\"

    -notarize-pass \t If given, the Apple account password. Otherwise an attempt will be macdeployqt_exists
\t\t\t to get the password from keychain using the account given in <notarize-ac> option.

    -asc-provider \t some AppleIds might need this option pass the <shortname>

    -style \t\t Style file defined from 'dmgstyle.sh' output

    -bg \t\t Set a background image for dmg folder.

    -name \t\t Set the DMG name output.

\t\t\t macos-notarize needs an input image to attach to the dmg background
\t\t\t image recommended size is at least 950x500
"
    exit 1
}


### Code Signature & NOTARIZATION
## set NOTARIZE to true if notarization args are present
check_notarization() {
    NOTARIZE="false"
    if [[ -z "${CODE_SIGNATURE}" ]]; then
        echo "WARNING: No code signature provided, Code will not be signed"
    else
        print_msg "Code will be signed with %s" "${CODE_SIGNATURE}"
        ### NOTARIZATION
        # check if we can perform notarization using notarytool
        xcrun notarytool history --keychain-profile KritaNotarizeAccount 1> /dev/null
        if [[ ${?} -eq 0 ]]; then
            NOTARYTOOL="short"
            NOTARIZE="true"
        fi

        if [[ ${NOTARIZE} = "false" && -n "${NOTARIZE_ACC}" ]]; then
            NOTARIZE="true"
            ASC_PROVIDER_OP=""

            if [[ -z ${APPLE_TEAMID} ]]; then
                echo "No team id provided, extracting from signature"
                APPLE_TEAMID=${CODE_SIGNATURE[-11,-2]}
            fi

            if [[ -n "${APPLE_TEAMID}" ]]; then
                ASC_PROVIDER_OP="--asc-provider ${APPLE_TEAMID}"
            fi

            if [[ -z "${NOTARIZE_PASS}" ]]; then
                NOTARIZE_PASS="@keychain:AC_PASSWORD"
                KEYCHAIN_PASS="true"
            fi

            # check if we can perform notarization
            
            xcrun notarytool history --apple-id "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" --team-id "${APPLE_TEAMID}" 1> /dev/null
            if [[ ${?} -ne 0 ]]; then
                echo "Unable to use notarytool: not setup/missing password, trying altool"
                ALTOOL="true"
                xcrun altool --notarization-history 0 --username "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" ${ASC_PROVIDER_OP} 1> /dev/null

                if [[ ${?} -ne 0 ]]; then
                    NOTARIZE="false"
                    echo "No password given for notarization or AC_PASSWORD missing in keychain"
                fi
            else
                NOTARYTOOL="long"
            fi
        fi
    fi

    if [[ ${NOTARIZE} = "true" ]]; then
        print_msg "Notarization checks complete, This build will be notarized"
    else
        echo "WARNING: Account information missing, Notarization will not be performed"
    fi
}

# Notarize build on macOS servers
# based on https://github.com/Beep6581/RawTherapee/blob/6fa533c40b34dec527f1176d47cc6c683422a73f/tools/osx/macosx_bundle.sh#L225-L250
notarize_build() {
    local NOT_SRC_DIR=$(get_absolute_path $(dirname ${1}))
    local NOT_SRC_FILE=${2}

    local notarization_complete="true"
    echo $NOT_SRC_DIR

    if [[ ${NOTARIZE} = "true" ]]; then
        printf "performing notarization of %s\n" "${2}"
        cd "${NOT_SRC_DIR}"

        ${SCRIPT_DEBUG} ditto -c -k --sequesterRsrc --keepParent "${NOT_SRC_FILE}" "./tmp_notarize/${NOT_SRC_FILE}.zip"

        if [[ ${NOTARYTOOL} = "short" ]]; then
            xcrun notarytool submit "./tmp_notarize/${NOT_SRC_FILE}.zip" --wait --keychain-profile KritaNotarizeAccount

        elif [[ ${NOTARYTOOL} = "long" ]]; then
            xcrun notarytool submit "./tmp_notarize/${NOT_SRC_FILE}.zip" --wait --apple-id "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" --team-id "${APPLE_TEAMID}"

        else
            # echo "xcrun altool --notarize-app --primary-bundle-id \"org.krita\" --username \"${NOTARIZE_ACC}\" --password \"${NOTARIZE_PASS}\" --file \"${BUILDROOT}/tmp_notarize/${NOT_SRC_FILE}.zip\""
            local altoolResponse="$(xcrun altool --notarize-app --primary-bundle-id "org.krita" --username "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" ${ASC_PROVIDER_OP} --file "./tmp_notarize/${NOT_SRC_FILE}.zip" 2>&1)"

            if [[ -n "$(grep 'Error' <<< ${altoolResponse})" ]]; then
                printf "ERROR: xcrun altool exited with the following error! \n\n%s\n\n" "${altoolResponse}"
                printf "This could mean there is an error in AppleID authentication!\n"
                printf "aborting notarization\n"
                NOTARIZE="false"
                return
            else
                printf "Response:\n\n%s\n\n" "${altoolResponse}"
            fi

            local uuid="$(grep 'RequestUUID' <<< ${altoolResponse} | awk '{ print $NF }')"
            echo "RequestUUID = ${uuid}" # Display identifier string

            waiting_fixed "Waiting to retrieve notarize status" 120

            while true ; do
                fullstatus=$(xcrun altool --notarization-info "${uuid}" --username "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" ${ASC_PROVIDER_OP} 2>&1)  # get the status
                notarize_status=`echo "${fullstatus}" | grep 'Status\:' | awk '{ print $2 }'`
                echo "${fullstatus}"
                if [[ "${notarize_status}" = "success" ]]; then
                    print_msg "Notarization success!"
                    break
                elif [[ "${notarize_status}" = "in" ]]; then
                    waiting_fixed "Notarization still in progress, wait before checking again" 60
                else
                    notarization_complete="false"
                    echo "Notarization failed! full status below"
                    echo "${fullstatus}"
                    exit 1
                fi
            done
        fi

        if [[ "${notarization_complete}" = "true" ]]; then
            xcrun stapler staple "${NOT_SRC_FILE}"   #staple the ticket
            xcrun stapler validate -v "${NOT_SRC_FILE}"
        fi
    fi
}

# helper to define function only once
batch_codesign() {
    local entitlements="${1}"
    if [[ -z "${1}" ]]; then
        entitlements="${KIS_SRC_DIR}/packaging/macos/entitlements.plist"
    fi
    xargs -P4 -I FILE codesign --options runtime --timestamp -f -s "${CODE_SIGNATURE}" --entitlements "${entitlements}" FILE
}

create_dmg () {
    local KRITA_DMG="${1}"
    local KRITA_APP_DIR=$(get_absolute_path "${KRITA_DMG}")
    printf "Creating of dmg with contents of %s...\n" "${KRITA_DMG}"
    cd ${KRITA_APP_DIR}/..
    DMG_size=1500

    ## Build dmg from folder

    # create dmg on local system
    # usage of -fsargs minimize gaps at front of filesystem (reduce size)
    hdiutil create -srcfolder "${KRITA_DMG}" -volname "${DMG_title}" -fs APFS \
        -format UDIF -verbose -size ${DMG_size}m krita.temp.dmg

    # Next line is only useful if we have a dmg as a template!
    # previous hdiutil must be uncommented
    # cp krita-template.dmg krita.dmg
    device=$(hdiutil attach -readwrite -noverify -noautoopen "krita.temp.dmg" | egrep '^/dev/' | sed 1q | awk '{print $1}')

    # rsync -priul --delete ${KRITA_DMG}/krita.app "/Volumes/${DMG_title}"

    # Set style for dmg
    if [[ ! -d "/Volumes/${DMG_title}/.background" ]]; then
        mkdir "/Volumes/${DMG_title}/.background"
    fi
    cp -v ${DMG_background} "/Volumes/${DMG_title}/.background/"

    mkdir "/Volumes/${DMG_title}/Terms of Use"
    cp -v "${KIS_SRC_DIR}/packaging/macos/Terms_of_use.rtf" "/Volumes/${DMG_title}/Terms of Use/"
    ln -s "/Applications" "/Volumes/${DMG_title}/Applications"

    ## Apple script to set style
    style="$(<"${DMG_STYLE}")"
    printf "${style}" "${DMG_title}" "${DMG_background##*/}" | osascript

    #Set Icon for DMG
    cp -v "${KIS_MACOS_PKGN_DIR}/KritaIcon.icns" "/Volumes/${DMG_title}/.VolumeIcon.icns"
    SetFile -a C "/Volumes/${DMG_title}"

    chmod -Rf go-w "/Volumes/${DMG_title}"

    # ensure all writing operations to dmg are over
    sync

    hdiutil detach $device
    hdiutil convert "krita.temp.dmg" -format UDZO -imagekey -zlib-level=9 -o krita-out.dmg


    mv krita-out.dmg ${DMG_NAME}
    echo "moved krita-out.dmg to ${DMG_NAME}"
    rm krita.temp.dmg

    if [[ -n "${CODE_SIGNATURE}" ]]; then
        printf "${DMG_NAME}" | batch_codesign
    fi

    echo "dmg done!"
}

### program starts

## parse args
# -- Parse input args
for arg in "${@}"; do
    if [[ ${arg} = -bg=* ]]; then
        DMG_validBG=0
        bg_filename=${arg#*=}
        echo "attempting to check background is valid jpg or png..."
        sips --getProperty format "${bg_filename}"
        BG_FORMAT=$(sips --getProperty format ${bg_filename} | awk '{printf $2}')

        if [[ "png" = ${BG_FORMAT} || "jpeg" = ${BG_FORMAT} ]];then
            echo "valid image file"
            DMG_background=$(cd "$(dirname "${bg_filename}")"; pwd -P)/$(basename "${bg_filename}")
            DMG_validBG=1
            # check imageDPI
            BG_DPI=$(sips --getProperty dpiWidth ${DMG_background} | grep dpi | awk '{print $2}')
            if [[ $(echo "${BG_DPI} > 150" | bc -l) -eq 1 ]]; then
            printf "WARNING: image dpi has an effect on apparent size!
    Check dpi is adequate for screen display if image appears very small
    Current dpi is: %s\n" ${BG_DPI}
            fi
        fi
    fi
    # If string starts with -sign

    if [[ ${arg} = -s=* ]]; then
        CODE_SIGNATURE="${arg#*=}"
    fi

    if [[ ${arg} = -f=* ]]; then
        INPUT_STR="${arg#*=}"
        INPUT_DIR="$(cd $(dirname ${INPUT_STR}); pwd -P)"
        KIS_APP_DIR="${INPUT_DIR}/$(basename ${INPUT_STR})"
    fi

    if [[ ${arg} = -name=* ]]; then
        DMG_NAME="${arg#*=}"
    fi

    if [[ ${arg} = -notarize-ac=* ]]; then
        NOTARIZE_ACC="${arg#*=}"
    fi

    if [[ ${arg} = -notarize-pass=* ]]; then
        NOTARIZE_PASS="${arg#*=}"
    fi

    if [[ ${arg} = -style=* ]]; then
        style_filename="${arg#*=}"
        if [[ -f "${style_filename}" ]]; then
            DMG_STYLE="${style_filename}"
        fi
    fi

    if [[ ${arg} = -asc-provider=* ]]; then
        APPLE_TEAMID="${arg#*=}"
    fi

    if [[ ${arg} = "-h" || ${arg} = "--help" ]]; then
        print_usage
    fi
done


check_notarization

detach_dmg

if [[ -z ${KIS_APP_DIR} ]]; then
    print_error "not input krita.app dir given!"
    print_usage
fi

if [[ -z "${DMG_NAME}" ]]; then
    # Add git version number
    DMG_NAME="krita-${KRITA_VERSION// /_}.dmg"
else
    DMG_NAME="${DMG_NAME%.dmg}.dmg"
fi

### STYLE for DMG
if [[ ! ${DMG_STYLE} ]]; then
    DMG_STYLE="${KIS_MACOS_PKGN_DIR}/default.style"
fi

print_msg "Using style from: %s" "${DMG_STYLE}"

### Background for DMG
if [[ ${DMG_validBG} -eq 0 ]]; then
    echo "No jpg or png valid file detected!!"
    echo "Using default style"
    DMG_background="${KIS_MACOS_PKGN_DIR}/krita_dmgBG.jpg"
fi

echo "## KIS_APP_DIR: ${KIS_APP_DIR}"
notarize_build "${KIS_APP_DIR}" "krita.app"

create_dmg ${KIS_APP_DIR}
notarize_build "${KIS_APP_DIR}" "${DMG_NAME}"