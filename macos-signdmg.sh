#!/usr/bin/env zsh

# script must be run in zsh
if [[ -z ${ZSH_VERSION} ]]; then
    echo "This script cannot be run using sh, use zsh."
    echo "exiting..."
    exit 1
fi

# TODO: remove need for BUILDROOT, instead ask for entitlements directly
#   As of now we use kis_source to find the location of the entitlements in krita source tree
SCRIPT_SOURCE_DIR="${0:a:h}"
source ${SCRIPT_SOURCE_DIR}/lib-macos.sh
source ${SCRIPT_SOURCE_DIR}/macos-sign-env.sh

print_usage () {
    printf "
macos-sign signs a krita.dmg outputing a krita.app ready for notarizedmg or a pkg for store

USAGE:
  macos-signdmg.sh [-f=<krita.dmg>][-s=<identity>] [-notarize-ac=<apple-account>]

    -f \t\t\t dmg input name
    -s \t\t\t Code sign identity for codesign

    -notarize-ac \t Apple account name for notarization purposes
\t\t\t script will attempt to get password from keychain, if fails provide one with
\t\t\t the -notarize-pass option: To add a password run

\t\t\t security add-generic-password -a \"AC_USERNAME\" -w <secret_password> -s \"AC_PASSWORD\"

    -notarize-pass \t If given, the Apple account password. Otherwise an attempt will be macdeployqt_exists
\t\t\t to get the password from keychain using the account given in <notarize-ac> option.

    -asc-provider \t some AppleIds might need this option pass the <shortname>

    --store \t\t sign and prepare dmg for store distribution. this also outputs a pkg file.

    -sins \t\t [store] developer install signature, for singnig pkg
"
    exit 1
}

waiting_fixed() {
    local message="${1}"
    local waitTime=${2}

    for i in $(seq ${waitTime}); do
        sleep 1
        printf -v dots '%*s' ${i}
        printf -v spaces '%*s' $((${waitTime} - $i))
        printf "\r%s [%s%s]" "${message}" "${dots// /.}" "${spaces}"
    done
    printf "\n"
}


### Code Signature & NOTARIZATION
## set NOTARIZE to true if notarization args are present
check_notarization() {
    NOTARIZE="false"
    if [[ -z "${CODE_SIGNATURE}" ]]; then
        print_error "No code signature provided, Code will not be signed"
        print_usage
    else
        print_msg "Code will be signed with %s" "${CODE_SIGNATURE}"
        ### NOTARIZATION
        # check if we can perform notarization using notarytool
        xcrun notarytool history --keychain-profile KritaNotarizeAccount 1> /dev/null
        if [[ ${?} -eq 0 ]]; then
            NOTARYTOOL="short"
            NOTARIZE="true"
        fi

        if [[ ${NOTARIZE} = "false" && -n "${NOTARIZE_ACC}" ]]; then
            NOTARIZE="true"
            ASC_PROVIDER_OP=""

            if [[ -z ${APPLE_TEAMID} ]]; then
                echo "No team id provided, extracting from signature"
                APPLE_TEAMID=${CODE_SIGNATURE[-11,-2]}
            fi

            if [[ -n "${APPLE_TEAMID}" ]]; then
                ASC_PROVIDER_OP="--asc-provider ${APPLE_TEAMID}"
            fi

            if [[ -z "${NOTARIZE_PASS}" ]]; then
                NOTARIZE_PASS="@keychain:AC_PASSWORD"
                KEYCHAIN_PASS="true"
            fi

            # check if we can perform notarization
            
            xcrun notarytool history --apple-id "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" --team-id "${APPLE_TEAMID}" 1> /dev/null
            if [[ ${?} -ne 0 ]]; then
                echo "Unable to use notarytool: not setup/missing password, trying altool"
                ALTOOL="true"
                xcrun altool --notarization-history 0 --username "${NOTARIZE_ACC}" --password "${NOTARIZE_PASS}" ${ASC_PROVIDER_OP} 1> /dev/null

                if [[ ${?} -ne 0 ]]; then
                    NOTARIZE="false"
                    echo "No password given for notarization or AC_PASSWORD missing in keychain"
                fi
            else
                NOTARYTOOL="long"
            fi
        fi
    fi

    if [[ ${NOTARIZE} = "true" ]]; then
        print_msg "Notarization checks complete, This build will be notarized"
    else
        echo "WARNING: Account information missing, Notarization will not be performed"
    fi
}

findEntitlementsFile() {
    if [[ -n ${1} ]]; then
        return
    fi

    local fileName="macStore-entitlements.plist"
    local subEntitlementsFile="sandboxdev_sub-entitlements.plist"

    if [[ -f "${DIR_CURRENT}/${fileName}" ]]; then
        KIS_ENTITLEMENTS="${DIR_CURRENT}/${fileName}"
        KIS_SUB_ENTITLEMENTS="${DIR_CURRENT}/${subEntitlementsFile}"
    elif [[ -f "${KIS_ENTITLEMENTS_DIR}/${fileName}" ]]; then
        KIS_ENTITLEMENTS="${KIS_ENTITLEMENTS_DIR}/${fileName}"
        KIS_SUB_ENTITLEMENTS="${KIS_ENTITLEMENTS_DIR}/${subEntitlementsFile}"
    fi
}

# calls /usr/libexec/PlistBuddy with basic args
# $1 file.plist to modify
# $2 -c command
# $3 plist key to modify
# $4 plist value for key
plistbuddy() {
    local plistbuddy="/usr/libexec/PlistBuddy"
    local filename="${1}"
    local cmd="${2}"
    local key="${3}"
    local value="${4}"
    
    ${SCRIPT_DEBUG} ${plistbuddy} "${filename}" -c "${cmd}:${key} ${value}"
}


modifyInfoPlistContents() {
    local plistbuddy="/usr/libexec/PlistBuddy"
    local PLIST_LOC="${KIS_DMG_WDIR}/krita.app/Contents"
    
    plistbuddy "${PLIST_LOC}/Info.plist" Set CFBundleIdentifier ${KIS_BUNDLE_ID}
    plistbuddy "${PLIST_LOC}/Info.plist" Set CFBundleVersion ${KIS_BUILD_VERSION}
    plistbuddy "${PLIST_LOC}/Info.plist" Set CFBundleShortVersionString ${KIS_VERSION}
    plistbuddy "${PLIST_LOC}/Info.plist" Set CFBundleLongVersionString ${KIS_VERSION}
    plistbuddy "${PLIST_LOC}/Info.plist" Set LSMinimumSystemVersion ${MACOSX_DEPLOYMENT_TARGET}
    plistbuddy "${PLIST_LOC}/Info.plist" Delete CFBundleGetInfoString

    plistbuddy "${PLIST_LOC}/Library/QuickLook/kritaquicklookng.appex/Contents/Info.plist" Set CFBundleIdentifier ${KIS_BUNDLE_ID}.quicklook
    # plistbuddy "${PLIST_LOC}/Library/Spotlight/kritaspotlight.mdimporter/Contents/Info.plist" Set CFBundleIdentifier ${KIS_BUNDLE_ID}.spotlight

    # plistbuddy "${PLIST_LOC}/Library/QuickLook/kritaquicklook.qlgenerator/Contents/Info.plist" Set LSMinimumSystemVersion ${MACOSX_DEPLOYMENT_TARGET}
    # plistbuddy "${PLIST_LOC}/Library/Spotlight/kritaspotlight.mdimporter/Contents/Info.plist" Set LSMinimumSystemVersion ${MACOSX_DEPLOYMENT_TARGET}
}

clean_dmg() {
    cd "${KIS_DMG_WDIR}/krita.app/Contents/MacOS"
    rm krita_version
    rm kritarunner
}

# helper to define function only once
batch_codesign() {
    local entitlements="${1}"
    if [[ -z "${1}" ]]; then
        entitlements=${KIS_ENTITLEMENTS}
        # ="${KIS_SRC_DIR}/packaging/macos/entitlements.plist"
    fi
    ${SCRIPT_DEBUG} xargs -P4 -I FILE codesign --options runtime --timestamp -f -s "${CODE_SIGNATURE}" --entitlements "${entitlements}" FILE
    # --generate-entitlement-der
}

# Code sign must be done as recommended by apple "sign code inside out in individual stages"
sign_bundle() {
    local KRITA_DMG=${1}
    cd ${KRITA_DMG}

    # sign Frameworks and libs
    cd ${KRITA_DMG}/krita.app/Contents/Frameworks
    # remove debug versions as both can't be signed at the same time.
    ${SCRIPT_DEBUG} rm $(find . -name "Qt*_debug")
    find ./Python.framework/ -name "*.o" -exec rm {} \;
    
    echo "fixing permission of dynamic libraries"
    find . -name "*.dylib" -and -not -perm +111 | xargs chmod a+x

    # Do not sign binaries inside frameworks except for Python's
    find . -type d -path "*.framework" -prune -false -o -perm +111 -not -type d | batch_codesign
    find Python.framework -type f -name "*.o" -or -name "*.so" -or -perm +111 -not -type d -not -type l | batch_codesign
    find . -type d -name "*.framework" | xargs printf "%s/Versions/Current\n" | batch_codesign

    # Sign all other files in Framework (needed)
    # there are many files in python do we need to sign them all?
    find krita-python-libs -type f | batch_codesign
    # find python -type f | batch_codesign

    # Sign only libraries and plugins
    cd ${KRITA_DMG}/krita.app/Contents/PlugIns
    find . -type f | batch_codesign

    cd ${KRITA_DMG}/krita.app/Contents/Library/QuickLook
    printf "kritaquicklook.qlgenerator" | batch_codesign

    cd ${KRITA_DMG}/krita.app/Contents/Library/Spotlight
    printf "kritaspotlight.mdimporter" | batch_codesign

    # It is necessary to sign every binary Resource file
    cd ${KRITA_DMG}/krita.app/Contents/Resources
    find . -perm +111 -type f | batch_codesign

    printf "${KRITA_DMG}/krita.app/Contents/MacOS/ffmpeg" | batch_codesign ${KIS_SUB_ENTITLEMENTS}
    printf "${KRITA_DMG}/krita.app/Contents/MacOS/ffprobe" | batch_codesign ${KIS_SUB_ENTITLEMENTS}
    
    printf "${KRITA_DMG}/krita.app/Contents/MacOS/kritarunner" | batch_codesign
    printf "${KRITA_DMG}/krita.app/Contents/MacOS/krita_version" | batch_codesign

    #Finally sign krita and krita.app
    printf "${KRITA_DMG}/krita.app/Contents/MacOS/krita" | batch_codesign
    printf "${KRITA_DMG}/krita.app" | batch_codesign
}

sign_hasError() {
    local KRITA_DMG="${1}"
    local CODESIGN_STATUS=1

    for f in $(find "${KRITA_DMG}" -type f); do
        if [[ -z $(file ${f} | grep "Mach-O") ]]; then
            continue
        fi

        CODESIGN_RESULT=$(codesign -vvv --strict ${f} 2>&1 | grep "not signed")

        if [[ -n "${CODESIGN_RESULT}" ]]; then
            CODESIGN_STATUS=0
            printf "${f} not signed\n" >&2
        fi
    done
    return ${CODESIGN_STATUS}
}


has_valid_entitlements() {
    local CODESIGN_STATUS=1
    local filename="${1}"

    if file_is_bin ${filename}; then
        local kis_entitlements=$(codesign -d --entitlements - "${filename}")
        if [[ -z $(stringContains ${kis_entitlements} "com.apple.security.app-sandbox") ]]; then
            echo "ERROR: missing entitlement for ${1}"
            CODESIGN_STATUS=0
        fi
    fi

    return ${CODESIGN_STATUS}
}


# returns true if file is binary
file_is_bin() {
    local ERROR_CODE=0
    local filename=${1}
    if [[ -z "$(file ${filename} | grep 'Mach-O')" ]]; then
        ERROR_CODE=1
    fi
    return ${ERROR_CODE}
}

### program starts
# TODO rename
kis_parse_args () {
    local CODE_SIGNATURE_OP=()
    local SIGN_DEV_INSTALL_OP=()
    local INPUT_STR_OP=()
    local DMG_NAME_OP=()
    local NOTARIZE_ACC_OP=()
    local NOTARIZE_PASS_OP=()
    local APPLE_TEAMID_OP=()
    local KIS_VERSION_OP=()
    local KIS_BUILD_VERSION_OP=()

    set -- ${@}
    zparseopts -K -E - \
        -store=SIGN_MACOS_STORE \
        s:=CODE_SIGNATURE_OP \
        sins:=SIGN_DEV_INSTALL_OP \
        {f,-file}:=INPUT_STR_OP \
        {n,-name,name}::=DMG_NAME_OP \
        {u,-username,notarize-ac}:=NOTARIZE_ACC_OP \
        {p,-password,notarize-pass}:=NOTARIZE_PASS_OP \
        {tid,-team-id,asc-provider}:=APPLE_TEAMID_OP \
        {kv,-kritaversion}:=KIS_VERSION_OP \
        {bv,-bundleversion}:=KIS_BUILD_VERSION_OP \
        {h,-help}=SHOW_HELP

    if [[ -n ${SHOW_HELP} ]]; then
        print_usage
    fi

    if [[ -n ${INPUT_STR_OP} ]]; then
        INPUT_STR=${INPUT_STR_OP[2]#=}
        INPUT_DIR="$(cd $(dirname ${INPUT_STR}); pwd -P)"
        INPUT_DMG="${INPUT_DIR}/$(basename ${INPUT_STR})"
    fi

    if [[ -n ${CODE_SIGNATURE_OP} ]]; then
        CODE_SIGNATURE=${CODE_SIGNATURE_OP[2]#=}
    fi

    if [[ -n ${SIGN_DEV_INSTALL_OP} ]]; then
        SIGN_DEV_INSTALL=${SIGN_DEV_INSTALL_OP[2]#=}
    fi

    if [[ -n ${DMG_NAME_OP} ]]; then
        DMG_NAME=${DMG_NAME_OP[2]#=}
    fi

    if [[ -n ${NOTARIZE_ACC_OP} ]]; then
        NOTARIZE_ACC=${NOTARIZE_ACC_OP[2]#=}
    fi

    if [[ -n ${NOTARIZE_PASS_OP} ]]; then
        NOTARIZE_PASS=${NOTARIZE_PASS_OP[-1]#=}
    fi

    if [[ -n ${APPLE_TEAMID_OP} ]]; then
        APPLE_TEAMID=${APPLE_TEAMID_OP[2]#=}
    fi

    if [[ -n ${KIS_BUILD_VERSION_OP} ]]; then
        KIS_BUILD_VERSION=${KIS_BUILD_VERSION_OP[2]#=}
    fi

    if [[ -n ${KIS_VERSION_OP} ]]; then
        KIS_VERSION=${KIS_VERSION_OP[2]#=}
    fi
}


kis_parse_args ${@}

echo "sign macstore ${SIGN_MACOS_STORE}"
echo "code sign ${CODE_SIGNATURE}"
echo "input dmg ${INPUT_DMG}"
echo "dmg_name ${DMG_NAME}"
echo "notirize account ${NOTARIZE_ACC}"
echo "notarize pass ${NOTARIZE_PASS}"
echo "apple teamid ${APPLE_TEAMID}"
echo "bundle version ${KIS_BUILD_VERSION}"


# preparation steps
# if [[ ! -d "${KIS_DMG_WDIR_DIRNAME}" ]]; then
#     mkdir "${KIS_DMG_WDIR_DIRNAME}"
#     KIS_DMG_WDIR=$(get_absolute_path ${KIS_DMG_WDIR_DIRNAME})
# fi
# KIS_DMG_WDIR=$(get_absolute_path ${KIS_DMG_WDIR_DIRNAME})

KIS_DMG_WDIR=$(pwd)
# cd "${KIS_DMG_WDIR}"
echo "cleaning last run..."
rm -rf "${KIS_DMG_WDIR}/krita.app"

if [[ -z ${INPUT_DMG} ]]; then
    print_error "not input dmg given!"
    print_usage
fi

echo "mounting ${INPUT_DMG} ..."
hdiutil attach "${INPUT_DMG}" -mountpoint "${KIS_DMG_MOUNT_NAME}"
rsync -prult --delete "${KIS_DMG_MOUNT_NAME}/krita.app/" "krita.app"
hdiutil detach "${KIS_DMG_MOUNT_NAME}"

# only for store
if [[ -n ${SIGN_MACOS_STORE} ]]; then
    # attach provisioning profile
    cp -X "${KIS_PROVISION}" "${KIS_DMG_WDIR}/krita.app/Contents/"
    chmod 644 "${KIS_DMG_WDIR}/krita.app/Contents/${KIS_PROVISION}"

    if [[ -z ${KIS_VERSION_OP} ]]; then
        # clean krita version string (app store format does not allow dashes
        KIS_VERSION="$(${KIS_DMG_WDIR}/krita.app/Contents/MacOS/krita_version 2> /dev/null | awk '{print $1}')"
        KIS_VERSION=${KIS_VERSION%%-*}
    fi
fi

# try to fix file permissions
for f in $(find ${KIS_DMG_WDIR} -not -perm +044); do
    chmod +r ${f}
done
rootfiles="$(find ${KIS_DMG_WDIR} -user root -perm +400 -and -not -perm +044)"
if [[ -n ${rootfiles} ]]; then
    echo "files \n ${rootfiles} \n cannot be read by non-root users!"
    echo "submission will fail, please fix and relaunch script"
fi


# only for store
# locate entitlements file to use
# findEntitlementsFile
if [[ -n ${SIGN_MACOS_STORE} ]]; then
    findEntitlementsFile ${KIS_ENTITLEMENTS}
    modifyInfoPlistContents
    clean_dmg
else 
    KIS_ENTITLEMENTS="${KIS_ENTITLEMENTS_DIR}/entitlements.plist"
    KIS_SUB_ENTITLEMENTS=""
fi

if [[ ! -e ${KIS_ENTITLEMENTS} ]]; then
    echo "Could not find entitlements file use for codesign"
    exit
else
    echo "using ${KIS_ENTITLEMENTS}"
fi

# actually perform codesign
if [[ -z ${CODE_SIGNATURE} ]]; then
    print_error "No certificate name given!"
    print_usage
fi

sign_bundle "${KIS_DMG_WDIR}"


if sign_hasError "${KIS_DMG_WDIR}" ; then
    print_error "CodeSign errors!"
    echo "stopping...."
    exit 1
fi
echo "no codesign errors"
echo "krita.app signed"


KRITA_DMG_BASENAME="$(basename ${INPUT_STR})"

if [[ -n ${SIGN_MACOS_STORE} ]]; then
    KRITA_PKG_NAME="${KRITA_DMG_BASENAME%.*}.pkg"
    # productbuild --component ${KIS_APPLOC}/krita.app  /Applications krita5_submit.pkg --sign \"3rd Party Mac Developer Installer: ...\" 
    if [[ -n "${SIGN_DEV_INSTALL}" ]]; then
        echo "creating ${KRITA_PKG_NAME} ..."
        productbuild --component "${KIS_DMG_WDIR}/krita.app"  "/Applications" "${KIS_DMG_WDIR}/${KRITA_PKG_NAME}" --sign "${SIGN_DEV_INSTALL}"
    fi

    echo "done"

    if [[ -n ${APPLE_TEAMID} ]]; then
        printf -v asc_provider_arg '--asc-provider "%s"' "${APPLE_TEAMID}"
    fi
    
    print_msg "
    Use altool to submit the Pkg:

        \t xcrun altool --upload-package "${KIS_DMG_WDIR}/${KRITA_PKG_NAME}" --bundle-id ${KIS_BUNDLE_ID} -t macos -u "${NOTARIZE_ACC}" ${asc_provider_arg} --bundle-version ${KIS_BUILD_VERSION} --bundle-short-version-string ${KIS_VERSION} --apple-id <app-id_fromStore>

    To get <app-id_fromStore> from store go to: https://appstoreconnect.apple.com/apps ,select the app you wish to upload to,
    click on App information on the left side, and search for 'Apple ID' field.

    \n"
fi
