#!/usr/bin/env zsh


# Helper functions
countArgs () {
    echo "${#}"
}

stringContains () {
    echo "$(grep "${2}" <<< "${1}")"
}

#recieves a path without filename
get_absolute_path() {
    local origLoc="$(pwd)"

    local filename="${1}"
    local location="${filename}"

    if [[ -e "${filename}" && ! -d "${filename}" ]]; then
        location="$(dirname "${filename}")"
    fi

    local absolute="$(cd "${location}"; pwd)"
    cd "${origLoc}"

    echo ${absolute}
}


# print status messages
print_msg() {
    printf "\e[32m${1}\e[0m\n" "${@:2}"
    # printf "%s\n" "${1}" >> ${OUTPUT_LOG}
}

# print error
print_error() {
    printf "\e[31m%s %s\e[0m\n" "ERROR:" "${1}"
}


# Debug helper function
debug_script() {
    echo "## running subcmd: ${@}"
    ${@}
}

detach_dmg() {
    # Attempt to detach previous mounted DMG
    if [[ -d "/Volumes/${DMG_title}" ]]; then
        echo "WARNING: Another Krita DMG is mounted!"
        echo "Attempting eject…"
        hdiutil detach "/Volumes/${DMG_title}"
        if [ $? -ne 0  ]; then
            exit 1
        fi
        echo "Success!"
    fi
}

#debug on
SCRIPT_DEBUG="debug_script"
