#!/usr/bin/env zsh

# macos-sign-env
DMG_title="krita" #if changed krita.temp.dmg must be deleted manually

KIS_SOURCE_DIRNAME="krita"
KIS_BUILD_DIRNAME="kisbuild"
KIS_INSTALL_DIRNAME="install"
KIS_DMG_DIRNAME="kritadmg"
KIS_DMG_TEMPLATE_DIRNAME="kritadmg-template"

KIS_DMG_WDIR_DIRNAME="kritadmg_store"
KIS_DMG_MOUNT_NAME="kritadmg"

MACOSX_DEPLOYMENT_TARGET="10.13"

#---
# check_buildroot
if [[ -z $BUILDROOT ]]; then
    echo "ERROR: BUILDROOT env not set!"
    echo "\t Must point to the root of the buildfiles as stated in 3rdparty Readme"
    echo "exiting..."
    exit 1
else
    # normalize path
    BUILDROOT=$(get_absolute_path "${BUILDROOT}")
fi


if [[ -z "${KIS_SRC_DIR}" ]]; then
    KIS_SRC_DIR=${BUILDROOT}/${KIS_SOURCE_DIRNAME}
fi

if [[ -z "${KIS_BUILD_DIR}" ]]; then
    KIS_BUILD_DIR=${BUILDROOT}/${KIS_BUILD_DIRNAME}
fi

KIS_INSTALL_DIR=${BUILDROOT}/${KIS_INSTALL_DIRNAME}

export PATH=${KIS_INSTALL_DIR}/bin:$PATH

# KRITA_DMG=${BUILDROOT}/${KIS_DMG_WDIR_DIRNAME}
# KRITA_DMG_TEMPLATE=${BUILDROOT}/${KIS_DMG_TEMPLATE_DIRNAME}

KIS_MACOS_PKGN_DIR=${KIS_SRC_DIR}/packaging/macos/
KIS_ENTITLEMENTS_DIR="${KIS_MACOS_PKGN_DIR}"
# we look for the provisioning file in the script the directory was called
KIS_PROVISION="$(get_absolute_path ".")/embedded.provisionprofile"
KIS_BUNDLE_ID="org.kde.krita"

KRITA_VERSION="$(${KIS_INSTALL_DIR}/bin/krita_version -v)"